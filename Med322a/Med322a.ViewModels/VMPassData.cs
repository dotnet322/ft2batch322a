﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMPassData
    {
        public VMPaging? pagination { get; set; }
        public VMVWDoctor? searchKey { get; set; }
        public List<VMVWDoctor>? listDoctor { get; set; }
        public string sort { get; set; }
        public int pageSize { get; set; }

    }
}
