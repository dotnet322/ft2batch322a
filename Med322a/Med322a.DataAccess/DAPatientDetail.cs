﻿using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.EntityFrameworkCore;
using Med322a.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DAPatientDetail
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DAPatientDetail(DB_MEDContext db) => this.db = db;

        private VMBiodata? GetIdBiodata(long id)
        {
            return (
                from b in db.MBiodata
                where b.Id == id && b.IsDelete == false
                select new VMBiodata
                {
                    Id = b.Id,
                    Fullname = b.Fullname,
                    MobilePhone = b.MobilePhone,
                    Image = b.Image,
                    ImagePath = b.ImagePath,

                    CreatedBy = b.CreatedBy,
                    CreatedOn = b.CreatedOn,
                    ModifiedBy = b.ModifiedBy,
                    ModifiedOn = b.ModifiedOn,
                    DeletedBy = b.DeletedBy,
                    DeletedOn = b.DeletedOn,
                    IsDelete = b.IsDelete,
                }).FirstOrDefault();
        }

        private VMCustomer? GetIdCustomer (long id)
        {
            return (
                from c in db.MCustomers
                join b in db.MBiodata on c.BiodataId equals b.Id
                where c.Id == id && c.IsDelete == false
                select new VMCustomer
                {
                    Id = c.Id,
                    BiodataId = b.Id,
                    Dob = c.Dob,
                    Gender = c.Gender,
                    BloodGroupId = c.BloodGroupId,
                    RhesusType = c.RhesusType,
                    Height = c.Height,
                    Weight = c.Weight,

                    CreatedBy = c.CreatedBy,
                    CreatedOn = c.CreatedOn,
                    ModifiedBy = c.ModifiedBy,
                    ModifiedOn = c.ModifiedOn,
                    DeletedBy = c.DeletedBy,
                    DeletedOn = c.DeletedOn,
                    IsDelete = c.IsDelete,
                }).FirstOrDefault();
        }

        private VMCustomerMember? GetMember (long id)
        {
            return (
                from cm in db.MCustomerMembers
                where cm.CustomerId == id && cm.IsDelete == false
                select new VMCustomerMember
                {
                    Id = cm.Id,
                    ParentBiodataId = cm.ParentBiodataId,
                    CustomerId = cm.CustomerId,
                    CustomerRelationId = cm.CustomerRelationId,

                    CreatedBy = cm.CreatedBy,
                    CreatedOn = cm.CreatedOn,
                    ModifiedBy = cm.ModifiedBy,
                    ModifiedOn = cm.ModifiedOn,
                    DeletedBy = cm.DeletedBy,
                    DeletedOn = cm.DeletedOn,
                    IsDelete = cm.IsDelete,
                }).FirstOrDefault();
        }

        public VMResponse GetByFilter(string filter, int Id)
        {
            try
            {
                List<VMPatientMember> data = (
                    from pmd in db.DMPatientMemberDetails.FromSqlRaw($"select * from vw_PatientMemberDetail where IsDelete = 0").ToList()
                    where pmd.UserId == Id && pmd.Fullname.ToLower().Contains(filter.ToLower())
                    select new VMPatientMember
                    {
                        UserId = pmd.UserId,
                        customerId = pmd.customerId,
                        memberId = pmd.memberId,
                        parentId = pmd.parentId,
                        BloodGroupId = pmd.BloodGroupId,
                        CustomerRelationId = pmd.CostumerRelationId,
                        Fullname = pmd.Fullname,
                        RelationName = pmd.RelationName,
                        Dob = pmd.Dob,
                        Gender = pmd.Gender,
                        RhesusType = pmd.RhesusType,
                        Height = pmd.Height,
                        Weight = pmd.Weight,
                        Age = pmd.Age,
                        Appointment = pmd.Appointment,
                        chat = pmd.chat,

                        CreatedBy = pmd.CreatedBy,
                        CreatedOn = pmd.CreatedOn,
                        ModifiedBy = pmd.ModifiedBy,
                        ModifiedOn = pmd.ModifiedOn,
                        DeletedBy = pmd.DeletedBy,
                        DeletedOn = pmd.DeletedOn,
                        IsDelete = pmd.IsDelete

                    }).ToList();

                response.success = true;
                response.data = data;
                return response;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
                response.data = null;
                return response;
            }
        }

        public VMResponse Create(VMPatientMember inputpatien)
        {
            VMResponse response = new VMResponse();
            try
            {
                // Tambahkan objek dataBio ke basis data
                MBiodatum dataBio = new MBiodatum
                {
                    Fullname = inputpatien.Fullname,
                    CreatedBy = (long)inputpatien.UserId,
                    CreatedOn = DateTime.Now
                };
                db.Add(dataBio);

                // Simpan perubahan ke basis data dan periksa apakah penyimpanan berhasil
                int savedChanges = db.SaveChanges();
                if (savedChanges > 0)
                {
                    // Simpan objek dataCustomer setelah dataBio berhasil disimpan
                    MCustomer dataCustomer = new MCustomer
                    {
                        BiodataId = dataBio.Id,
                        BloodGroupId = inputpatien.BloodGroupId,
                        Dob = inputpatien.Dob,
                        Gender = inputpatien.Gender,
                        RhesusType = inputpatien.RhesusType,
                        Height = inputpatien.Height,
                        Weight = inputpatien.Weight,
                        CreatedBy = (long)inputpatien.UserId,
                        CreatedOn = DateTime.Now
                    };
                    db.Add(dataCustomer);

                    // Simpan perubahan dan periksa apakah penyimpanan berhasil
                    int savedChanges2 = db.SaveChanges();
                    if (savedChanges2 > 0)
                    {

                        // Simpan objek dataCustomerMember setelah dataCustomer berhasil disimpan
                        MCustomerMember dataCustomerMember = new MCustomerMember
                        {
                            ParentBiodataId = inputpatien.parentId,
                            CustomerRelationId = inputpatien.CustomerRelationId,
                            CustomerId = dataCustomer.Id,
                            CreatedBy = dataBio.CreatedBy,
                            CreatedOn = DateTime.Now
                        };
                        db.Add(dataCustomerMember);

                        // Simpan perubahan lagi setelah dataCustomerMember ditambahkan
                        int savedChanges3 = db.SaveChanges();
                        if (savedChanges3 > 0)
                        {
                            response.data = GetPasienMemberByUserId((int)inputpatien.UserId);
                            response.message = "Patient successfully Added/Updated!";
                        }
                        else
                        {
                            response.message = "Failed to save dataCustomerMember.";
                            response.success = false;
                        }
                    }
                    else
                    {
                        response.message = "Failed to save dataCustomer.";
                        response.success = false;
                    }
                }
                else
                {
                    response.message = "Failed to save dataBio.";
                    response.success = false;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse GetPasienMemberByUserId(int userId)
        {
            try
            {
                List<VMPatientMember?> data = (
                    from patientMemberDetail in db.DMPatientMemberDetails.FromSqlRaw(
                        $"select * from vw_PatientMemberDetail where userId={userId} and IsDelete=0").ToList()

                    select new VMPatientMember
                    {
                        UserId = patientMemberDetail.UserId,
						customerId = patientMemberDetail.customerId,
						parentId = patientMemberDetail.parentId,
                        memberId = patientMemberDetail.memberId,
						BloodGroupId = patientMemberDetail.BloodGroupId,
                        CustomerMemberId = patientMemberDetail.CustomerMemberId,
						CustomerRelationId = patientMemberDetail.CostumerRelationId,
                        Fullname = patientMemberDetail.Fullname,
						RelationName = patientMemberDetail.RelationName,
						Dob = patientMemberDetail.Dob,
						Gender = patientMemberDetail.Gender,
						RhesusType = patientMemberDetail.RhesusType,
						Height = patientMemberDetail.Height,
						Weight = patientMemberDetail.Weight,
						Age = patientMemberDetail.Age,
						Appointment = patientMemberDetail.Appointment,
						chat = patientMemberDetail.chat,

						CreatedBy = patientMemberDetail.CreatedBy,
						CreatedOn = patientMemberDetail.CreatedOn,
						ModifiedBy = patientMemberDetail.ModifiedBy,
						ModifiedOn = patientMemberDetail.ModifiedOn,
						DeletedBy = patientMemberDetail.DeletedBy,
						DeletedOn = patientMemberDetail.DeletedOn,
						IsDelete = patientMemberDetail.IsDelete
					}
                    ).ToList();
                response.data = data;
            }catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMPatientMember GetEachPasienMember2(int biodataId)
        {
            VMPatientMember data = null;

            try
            {
                data = (
                    from patientMemberDetail in db.DMPatientMemberDetails.FromSqlRaw(
                        $"select * from vw_PatientMemberDetail where memberId={biodataId} and IsDelete=0").ToList()

                    select new VMPatientMember
                    {
                        UserId = patientMemberDetail.UserId,
                        customerId = patientMemberDetail.customerId,
                        parentId = patientMemberDetail.parentId,
                        memberId = patientMemberDetail.memberId,
                        BloodGroupId = patientMemberDetail.BloodGroupId,
                        CustomerMemberId = patientMemberDetail.CustomerMemberId,
                        CustomerRelationId = patientMemberDetail.CostumerRelationId,
                        Fullname = patientMemberDetail.Fullname,
                        RelationName = patientMemberDetail.RelationName,
                        Dob = patientMemberDetail.Dob,
                        Gender = patientMemberDetail.Gender,
                        RhesusType = patientMemberDetail.RhesusType,
                        Height = patientMemberDetail.Height,
                        Weight = patientMemberDetail.Weight,
                        Age = patientMemberDetail.Age,
                        Appointment = patientMemberDetail.Appointment,
                        chat = patientMemberDetail.chat,

                        CreatedBy = patientMemberDetail.CreatedBy,
                        CreatedOn = patientMemberDetail.CreatedOn,
                        ModifiedBy = patientMemberDetail.ModifiedBy,
                        ModifiedOn = patientMemberDetail.ModifiedOn,
                        DeletedBy = patientMemberDetail.DeletedBy,
                        DeletedOn = patientMemberDetail.DeletedOn,
                        IsDelete = patientMemberDetail.IsDelete
                    }
                ).FirstOrDefault();
            }
            catch (Exception ex)
            {
                // Handle the exception if needed.
            }

            return data;
        }
        //ini pake VMResponse
        public VMResponse GetEachPasienMember(int biodataId)
        {
            try
            {
                VMPatientMember data = (
                    from patientMemberDetail in db.DMPatientMemberDetails.FromSqlRaw(
                        $"select * from vw_PatientMemberDetail where memberId={biodataId} and IsDelete=0").ToList()

                    select new VMPatientMember
                    {
                        UserId = patientMemberDetail.UserId,
                        customerId = patientMemberDetail.customerId,
                        parentId = patientMemberDetail.parentId,
                        memberId = patientMemberDetail.memberId,
                        BloodGroupId = patientMemberDetail.BloodGroupId,
                        CustomerMemberId = patientMemberDetail.CustomerMemberId,
                        CustomerRelationId = patientMemberDetail.CostumerRelationId,
                        Fullname = patientMemberDetail.Fullname,
                        RelationName = patientMemberDetail.RelationName,
                        Dob = patientMemberDetail.Dob,
                        Gender = patientMemberDetail.Gender,
                        RhesusType = patientMemberDetail.RhesusType,
                        Height = patientMemberDetail.Height,
                        Weight = patientMemberDetail.Weight,
                        Age = patientMemberDetail.Age,
                        Appointment = patientMemberDetail.Appointment,
                        chat = patientMemberDetail.chat,

                        CreatedBy = patientMemberDetail.CreatedBy,
                        CreatedOn = patientMemberDetail.CreatedOn,
                        ModifiedBy = patientMemberDetail.ModifiedBy,
                        ModifiedOn = patientMemberDetail.ModifiedOn,
                        DeletedBy = patientMemberDetail.DeletedBy,
                        DeletedOn = patientMemberDetail.DeletedOn,
                        IsDelete = patientMemberDetail.IsDelete
                    }
                    ).FirstOrDefault();
                response.data = data;
                response.message = "data successfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        //INI CARA NASRI
        //public VMPatientMember? GetDetailPasienMember(long pasienId)
        //{
        //    var pmdQuery = db.DMPatientMemberDetails.FromSqlRaw($"select * from vw_PatientMemberDetail where customerId = {pasienId} IsDelete = 0");
        //    var pmd = pmdQuery.FirstOrDefault();
        //    if (pmd != null)
        //    {
        //        var result = new VMPatientMember
        //        {
        //            customerId = pmd.customerId,
        //            parentId = pmd.parentId,
        //            memberId = pmd.memberId,
        //            BloodGroupId = pmd.BloodGroupId,
        //            CustomerRelationId = pmd.CostumerRelationId,
        //            Fullname = pmd.Fullname,
        //            RelationName = pmd.RelationName,
        //            Dob = pmd.Dob,
        //            Gender = pmd.Gender,
        //            RhesusType = pmd.RhesusType,
        //            Height = pmd.Height,
        //            Weight = pmd.Weight,
        //            Age = pmd.Age,
        //            Appointment = pmd.Appointment,
        //            chat = pmd.chat,

        //            CreatedBy = pmd.CreatedBy,
        //            CreatedOn = pmd.CreatedOn,
        //            ModifiedBy = pmd.ModifiedBy,
        //            ModifiedOn = pmd.ModifiedOn,
        //            DeletedBy = pmd.DeletedBy,
        //            DeletedOn = pmd.DeletedOn,
        //            IsDelete = pmd.IsDelete
        //        };
        //        return result;
        //    }
        //    return null;
        //}

        public VMResponse EditPasienMember(VMPatientMember member)
        {
            try
            {
                //edit biodata (namanya aja)
                MBiodatum dataBiodataKosong = new MBiodatum();
                dataBiodataKosong.Fullname = member.Fullname;

                //edit beberapa field dari customer
                MCustomer dataCustomerKosong = new MCustomer();
                dataCustomerKosong.Dob = member.Dob;
                dataCustomerKosong.Gender = member.Gender;
                dataCustomerKosong.BloodGroupId = member.BloodGroupId;
                dataCustomerKosong.RhesusType = member.RhesusType;
                dataCustomerKosong.Weight = member.Weight;
                dataCustomerKosong.Height = member.Height;

                //update modified by dan modified on di table customer_member
                MCustomerMember dataCustomerMemberKosong = new MCustomerMember();
                dataCustomerMemberKosong.ModifiedBy = member.UserId;
                dataCustomerMemberKosong.ModifiedOn = DateTime.Now;

                if (member == null)
                {
                    response.success = false;
                    response.message = "Patient data is not Exist!";
                    return response;
                } else
                {
                    VMBiodata biodataLookUp = GetIdBiodata((long)member.memberId);

                    //edit biodata
                    dataBiodataKosong.Id = biodataLookUp.Id;
                    dataBiodataKosong.Image = biodataLookUp.Image;
                    dataBiodataKosong.ImagePath = biodataLookUp.ImagePath;

                    //edit base Properties biodata
                    dataBiodataKosong.CreatedBy = biodataLookUp.CreatedBy;
                    dataBiodataKosong.CreatedOn = biodataLookUp.CreatedOn;
                    dataBiodataKosong.ModifiedBy = member.UserId;
                    dataBiodataKosong.ModifiedOn = DateTime.Now;
                    dataBiodataKosong.DeletedBy = biodataLookUp.DeletedBy;
                    dataBiodataKosong.DeletedOn = biodataLookUp.DeletedOn;
                    dataBiodataKosong.IsDelete = biodataLookUp.IsDelete;

                    db.Update(dataBiodataKosong);

                    VMCustomer customerLookUp = GetIdCustomer((long)member.memberId);
                    //edit customer
					dataCustomerKosong.Id = customerLookUp.Id;
					dataCustomerKosong.BiodataId = customerLookUp.BiodataId;

					//edit base Properties customer
					dataCustomerKosong.CreatedBy = customerLookUp.CreatedBy;
					dataCustomerKosong.CreatedOn = customerLookUp.CreatedOn;
					dataCustomerKosong.ModifiedBy = member.UserId;
					dataCustomerKosong.ModifiedOn = DateTime.Now;
					dataCustomerKosong.DeletedBy = customerLookUp.DeletedBy;
					dataCustomerKosong.DeletedOn = customerLookUp.DeletedOn;
					dataCustomerKosong.IsDelete = customerLookUp.IsDelete;

                    db.Update(dataCustomerKosong);

                    VMCustomerMember customerMemberLookUp = GetMember((long)member.customerId);

                    dataCustomerMemberKosong.Id = customerMemberLookUp.Id;
                    dataCustomerMemberKosong.ParentBiodataId = customerMemberLookUp.ParentBiodataId;
                    dataCustomerMemberKosong.CustomerId = customerMemberLookUp.CustomerId;
                    dataCustomerMemberKosong.CustomerRelationId = customerMemberLookUp.CustomerRelationId;
                    dataCustomerMemberKosong.CreatedBy = customerMemberLookUp.CreatedBy;
                    dataCustomerMemberKosong.CreatedOn = customerMemberLookUp.CreatedOn;
                    dataCustomerMemberKosong.DeletedBy = customerMemberLookUp.DeletedBy;
                    dataCustomerMemberKosong.DeletedOn = customerMemberLookUp.DeletedOn;
                    dataCustomerMemberKosong.IsDelete = customerMemberLookUp.IsDelete;

                    db.Update(dataCustomerMemberKosong);

                    response.message = "Biodata berhasil di update";
                }
                db.SaveChanges();
                response.data = member;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
                response.data = null;
            }
            return response;
        }

        public VMResponse Delete(int memberId)
        {
            try
            {
                VMPatientMember? data = GetEachPasienMember2(memberId);

                MBiodatum biodata = new MBiodatum();
                biodata.Id = (long)data.memberId;
                biodata.Fullname = data.Fullname;
                biodata.CreatedBy = data.CreatedBy;
                biodata.CreatedOn = data.CreatedOn;
                biodata.ModifiedBy = data.ModifiedBy;
                biodata.ModifiedOn = data.ModifiedOn;
                biodata.DeletedBy = data.UserId;
                biodata.DeletedOn = DateTime.Now;
                biodata.IsDelete = true;

                db.Update(biodata);

                MCustomer customer = new MCustomer();
                customer.Id = (long)data.customerId;
                customer.BiodataId = data.memberId;
                customer.Dob = data.Dob;
                customer.Gender = data.Gender;
                customer.BloodGroupId = data.BloodGroupId;
                customer.RhesusType = data.RhesusType;
                customer.Height = data.Height;
                customer.Weight = data.Weight;

                customer.CreatedBy = data.CreatedBy;
                customer.CreatedOn = data.CreatedOn;
                customer.ModifiedBy = data.ModifiedBy;
                customer.ModifiedOn = data.ModifiedOn;
                customer.DeletedBy = data.UserId;
                customer.DeletedOn = DateTime.Now;
                customer.IsDelete = true;

                db.Update(customer);
                MCustomerMember customerMember = new MCustomerMember();
                customerMember.Id = (long)data.CustomerMemberId;
                customerMember.ParentBiodataId = data.parentId;
                customerMember.CustomerId = data.customerId;
                customerMember.CustomerRelationId = data.CustomerRelationId;
                customerMember.CreatedBy = data.CreatedBy;
                customerMember.CreatedOn = data.CreatedOn;
                customerMember.ModifiedBy = data.ModifiedBy;
                customerMember.ModifiedOn = data.ModifiedOn;
                customerMember.DeletedBy = data.UserId;
                customerMember.DeletedOn = DateTime.Now;
                customerMember.IsDelete = true;

                db.Update(customerMember);

                // Simpan perubahan ke database
                db.SaveChanges();

                response.message = "Patient successfully updated!";
            }
            catch (Exception ex)
            {
                throw;
            }

            return response;
        }
    }
}
