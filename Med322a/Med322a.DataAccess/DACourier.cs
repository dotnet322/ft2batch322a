﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
	public class DACourier
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DACourier(DB_MEDContext _db)
		{
			db = _db;
		}

		private VMMCourier? GetById(long id)
		{
			return (
					from mc in db.MCouriers
					where mc.Id == id && mc.IsDelete == false
					select new VMMCourier
					{
						Id = mc.Id,
						Name = mc.Name,
						
						IsDelete = mc.IsDelete,

						CreatedBy = mc.CreatedBy,
						CreatedOn = mc.CreatedOn,
						ModifiedBy = mc.ModifiedBy,
						ModifiedOn = mc.ModifiedOn,
						DeletedBy = mc.DeletedBy,
						DeletedOn = mc.DeletedOn,
					}
				).FirstOrDefault();
		}
		public VMResponse GetAll()
		{
			try
			{
				List<VMMCourier> data = (
					from mc in db.MCouriers
					where mc.IsDelete == false
					select new VMMCourier
					{
						Id = mc.Id,
						Name = mc.Name,

						IsDelete = mc.IsDelete,

						CreatedBy = mc.CreatedBy,
						CreatedOn = mc.CreatedOn,
						ModifiedBy = mc.ModifiedBy,
						ModifiedOn = mc.ModifiedOn,
						DeletedBy = mc.DeletedBy,
						DeletedOn = mc.DeletedOn,
					}
				).ToList();
				if (data.Count < 1)
				{
					response.message = "Kurir table has no data!";
					response.success = false;
					return response;
				}

				response.data = data;
				response.message = "Kurir data successsfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

		public VMResponse Get(long id)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Kurir ID was submitted!";

					return response;
				}

				response.data = GetById(id);

				if (response.data == null)
				{
					response.message = $"Kurir data with ID:{id} is not exist!";
					response.success = false;
				}
				else
					response.message = $"Kurir data with ID:{id} is successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

		public VMResponse GetByFilter(string filter)
		{
			try
			{
				List<VMMCourier> role = (
					from mc in db.MCouriers
					where mc.IsDelete == false
					select new VMMCourier
					{
						Id = mc.Id,
						Name = mc.Name,

						IsDelete = mc.IsDelete,

						CreatedBy = mc.CreatedBy,
						CreatedOn = mc.CreatedOn,
						ModifiedBy = mc.ModifiedBy,
						ModifiedOn = mc.ModifiedOn,
						DeletedBy = mc.DeletedBy,
						DeletedOn = mc.DeletedOn,
					}
			   ).ToList();

				response.message = (role.Count > 0)
					? "Kurir data successfully fetched!"
					: "No Menu found!";

				response.success = (role.Count > 0) ? true : false;
				response.data = role;
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = "Failed to fetched Kurir data!" + ex.Message;
			}
			return response;
		}
	}
}
