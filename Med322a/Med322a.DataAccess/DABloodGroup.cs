﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DABloodGroup
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DABloodGroup (DB_MEDContext db) => this.db = db;

        public VMResponse GetAllBlood()
        {
            try
            {
                    List<VMTblMBloodGroup> data = (
                    from bg in db.MBloodGroups
                    where bg.IsDelete == false
                    select new VMTblMBloodGroup
                    {
                        Id = bg.Id,
                        Code = bg.Code,
                        Description = bg.Description,
                        CreatedBy = bg.CreatedBy,
                        CreatedOn = bg.CreatedOn,
                        ModifiedBy = bg.ModifiedBy,
                        ModifiedOn = bg.ModifiedOn,
                        DeletedBy = bg.DeletedBy,
                        DeletedOn = bg.DeletedOn,
                        IsDelete = bg.IsDelete
                    }
                    ).ToList();
                response.data = (data.Count < 1) ? null : data;
                response.message = (response.data == null) ? "No Data" : "Product data successfully fetched!";
            }catch (Exception ex)
            {
                response.data = null;
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
